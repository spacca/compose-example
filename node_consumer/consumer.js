const rabbit = require('amqplib');
const MongoClient = require('mongodb').MongoClient;

const QUEUE_NAME = 'votes';
const RABBIT_URL = 'amqp://rabbitmq';

async function connectRabbitMQ() {
    try {
        const connection = await rabbit.connect(RABBIT_URL);

        connection.on("close", function () {
            console.error("[AMQP] reconnecting");
            setTimeout(connectRabbitMQ, 1000)
        });

        const channel = await connection.createChannel();

        await channel.assertQueue(QUEUE_NAME);

        channel.consume(QUEUE_NAME, (m) => {
            console.log(`received message ${m.content.toString()}`)
            writeToDB(m.content.toString());
            channel.ack(m)
        })
        console.log("RabbitMQ connected")

    } catch (error) {
        console.log("error during connection, will try again")
        setTimeout(connectRabbitMQ, 1000)
    }
}

const MONGO_URL = "mongodb://mongo:27017/";
const DB_NAME = 'catvsdog'
const COLLECTION_NAME = 'catvsdogc'
let mongodb = null

function connectMongoDb() {
    MongoClient.connect(MONGO_URL, (err, db) => {
        if (err) {
            setTimeout(connectMongoDb, 1000);
        };
        db.on('close', function () {
            mongodb = null
            setTimeout(connectMongoDb, 1000);
            console.log('MongoDB Error...close');
        });
        console.log("MongoDB connected")
        mongodb = db
    });
}

function writeToDB(vote) {
    if (mongodb) {
        const dbo = mongodb.db(DB_NAME);
        const voteobj = { vote };

        dbo.collection(COLLECTION_NAME).insertOne(voteobj, function (err, res) {
            if (err) throw err;
        });
    }

}


connectRabbitMQ();
connectMongoDb();