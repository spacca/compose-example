If you want to push new images you will have to change the prefix "spacca" everywhere or the login will fail.

run.sh will compile and push the application on docker hub, after the build is complete the script will also execute the containers.

If you want to just execute the application you only need docker-compose.yml, copy it somewhere and run it using "docker-compose up"

to run on kubernetes you can use kubernetes-run-sh

URL when running using kubernetes
http://kubernetes.docker.internal

URL when running using docker-compose
http://localhost:8080