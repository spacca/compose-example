import './App.css';
import { ReactComponent as CatSvg } from './cat.svg';
import { ReactComponent as DogSvg } from './dog.svg';
import React, { useEffect, useState } from 'react';
import { w3cwebsocket as W3CWebSocket } from "websocket";
import axios from 'axios';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import '@fontsource/roboto/700.css';



function App() {

  const [votes, setVotes] = useState({ cats: '0', dogs: '0' })



  useEffect(() => {

    const client = new W3CWebSocket(`ws://${window.location.host}/ws`);

    client.onopen = () => {
      console.log('WebSocket Client Connected');
    };

    client.onmessage = (message) => {
      setVotes(JSON.parse(message.data))
    };

    return () => client.close()

  }, []);

  const voteCat = () => {
    axios({
      method: 'post',
      url: '/api/cats',
      data: {}
    });
  }

  const voteDog = () => {
    axios({
      method: 'post',
      url: '/api/dogs',
      data: {}
    });
  }

  return (
    <div >
      <Stack spacing={2} direction="row" justifyContent='center' divider={<Divider orientation="vertical" flexItem />}>

        <Stack spacing={2} direction="column" alignItems="center">
          <Typography variant="h1" component="div" gutterBottom>
            {votes.cats}
          </Typography>
          <CatSvg style={{ height: '100px' }} />
          <Button variant='contained' onClick={voteCat}>
            cat
          </Button>
        </Stack>

        <Stack spacing={2} direction="column" alignItems="center">
          <Typography variant="h1" component="div" gutterBottom>
            {votes.dogs}
          </Typography>
          <DogSvg style={{ height: '100px' }} />
          <Button variant='contained' onClick={voteDog}>
            dog
          </Button>
        </Stack>

      </Stack>


    </div>

  );
}

export default App;
