const rab = require('amqplib');

const port = 8000;
const QUEUE_NAME = 'votes';
const EXCHANGE_TYPE = 'direct';
const EXCHANGE_NAME = 'main';
const KEY = 'myKey';
const HOST = 'amqp://rabbitmq';

async function sendMessageToQueue(message) {
    connection = rab.connect(HOST);
    connection.then(async (conn) => {
        const channel = await conn.createChannel();
        await channel.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE);
        await channel.assertQueue(QUEUE_NAME);
        channel.bindQueue(QUEUE_NAME, EXCHANGE_NAME, KEY);
        channel.sendToQueue(QUEUE_NAME, Buffer.from(message))
    })
}



const MongoClient = require('mongodb').MongoClient;

const MONGO_URL = "mongodb://mongo:27017/";
const DB_NAME = 'catvsdog'
const COLLECTION_NAME = 'catvsdogc'
let mongodb = null

function connectMongoDb() {
    MongoClient.connect(MONGO_URL, (err, db) => {
        if (err) {
            setTimeout(connectMongoDb, 1000);
        };
        db.on('close', function () {
            mongodb = null
            setTimeout(connectMongoDb, 1000);
            console.log('MongoDB Error...close');
        });
        console.log("MongoDB connected")
        mongodb = db
    });
}

connectMongoDb()


async function getCatVotes() {
    if (!mongodb) return 0;
    cursor = mongodb.db(DB_NAME).collection(COLLECTION_NAME).find(
        {
            vote: { $eq: 'cat' },
        }
    );
    const results = await cursor.toArray();
    return results.length
}

async function getDogVotes() {
    if (!mongodb) return 0;
    cursor = mongodb.db(DB_NAME).collection(COLLECTION_NAME).find(
        {
            vote: { $eq: 'dog' },
        }
    );
    const results = await cursor.toArray();
    return results.length

}


const WebSocket = require('ws').WebSocket
const WebSocketServer = require('ws').WebSocketServer
const wss = new WebSocketServer({ port: 8080 });

async function broadcastVotes() {
    dogs = await getDogVotes();
    cats = await getCatVotes();

    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify({ cats, dogs }), { binary: false });
        }
    });

}

setInterval(broadcastVotes, 1000)



const express = require("express")
var cors = require('cors')
const exp = express();

exp.use(cors())

exp.post('/cats', async (req, res) => {
    await sendMessageToQueue('cat')
    res.status(201).send('""');
})

exp.post('/dogs', async (req, res) => {
    await sendMessageToQueue('dog')
    res.status(201).send('""');
})

exp.listen(port, () => {
    console.log('listen port 8000');
})