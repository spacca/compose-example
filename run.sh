#!/bin/bash

npm --prefix ./node_api install ./node_api
npm --prefix ./node_consumer install ./node_consumer
npm --prefix ./react_ui install ./react_ui
npm --prefix ./react_ui run build

docker image build ./node_api -t spacca/node-api:latest
docker image build ./node_consumer -t spacca/node-consumer
docker image build ./react_ui -t spacca/react-ui:latest

docker login

docker image push spacca/node-api:latest
docker image push spacca/node-consumer:latest
docker image push spacca/react-ui:latest

docker-compose up -d